<?php

/**
 * @file
 * Admin page and form callbacks.
 */

/**
 * Callback for the Personalize Pages list page.
 */
function personalize_view_modes_list() {
  $header = array(
    array('data' => t('Variation set name')),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $rows = array();

  foreach (personalize_option_set_load_by_type('view_modes') as $option_set) {
    $tablerow = array(
      array('data' => check_plain($option_set->label)),
      array('data' => l(t('Edit'), 'admin/structure/personalize/variations/personalize-view-modes/manage/'. $option_set->osid . '/edit')),
      array('data' => l(t('Delete'), 'admin/structure/personalize/variations/personalize-view-modes/manage/'. $option_set->osid . '/delete')),
    );
    $rows[] = $tablerow;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No view_modes available.'), 'colspan' => 3));
  }

  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'personalize_view_modes'),
  );
  return $build;
}

/**
 * Form callback for the Personalize Page add/edit form.
 */
function personalize_view_modes_form($form, &$form_state, $formtype, $variant_set = NULL) {

  // When the form is in edit mode, the option set is passed as a param.
  if (!isset($variant_set)) {
    if (!empty($form_state['variant_set'])) {
      $variant_set = $form_state['variant_set'];
    }
    else {
      $variant_set = new stdClass;
      // If creating a new personalize_view_modes instance, start off with 2 empty view modes.
        $variant_set->options = array(
          0 => array('option_label' => personalize_generate_option_label(0)),
          1 => array('option_label' => personalize_generate_option_label(1)),
        );
     }
  }
  $form_state['variant_set'] = $variant_set;

  if (isset($form_state['values']['title'])) {
    $variant_set =_personalize_view_modes_form_to_personalized_view_mode($form_state);
  }

  if (!empty($variant_set->osid)) {
    // Make sure a warning message is delivered if this is a running
    // campaign.
//    personalize_warn_if_running($variant_set->agent);
    $form['osid'] = array(
      '#type' => 'value',
      '#value' => $variant_set->osid,
    );
    $form['agent_select'] = array(
      '#type' => 'value',
      '#value' => $variant_set->agent,
    );
  }
  else {
    $current_agent = isset($variant_set->agent) ? $variant_set->agent : '';
    $form += personalize_get_agent_selection_form($current_agent, TRUE, TRUE);
  }

  $entity_type_default = isset($variant_set->data['entity_type']) ? $variant_set->data['entity_type'] : '';
  $bundle_default = isset($variant_set->data['bundle']) ? $variant_set->data['bundle'] : '';
  $entity_id_default = isset($variant_set->data['entity_id']) ? $variant_set->data['entity_id'] : '';
  if($entity_type_default && $entity_id_default) {
    $entity = entity_load_single($entity_type_default, $entity_id_default);
    $entity_id_default = trim(entity_label($entity_type_default, $entity) . " [$entity_id_default]");
  }

  $form['title'] = array(
    '#title' => t('Title'),
    '#description' => t('This will be the administrative title of the variant.'),
    '#type' => 'textfield',
    '#default_value' => isset($variant_set->label) ? $variant_set->label : '',
    '#required' => TRUE,
  );
  $form['entity_type'] = array(
    '#title' => t('Entity Type'),
    '#type' => 'select',
    '#options' => _personalize_view_modes_get_entity_type_options(),
    '#empty_value' => '',
    '#empty_option' => '- Select -',
    '#default_value' => $entity_type_default,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'personalize_view_modes_form_ajax_callback',
      'event' => 'change blur',
    ),
  );

  $form['bundle'] = array(
    '#type' => 'select',
    '#title' => 'Bundle',
    '#prefix' => '<div id="variant-set-bundle-ajax-wrapper">',
    '#suffix' => '</div>',
    '#required' => TRUE,
    // When the form is rebuilt during ajax processing, the $selected variable
    // will now have the new value and so the options will change.
    '#options' => _personalize_view_modes_get_bundle_options($entity_type_default),
    '#empty_value' => '',
    '#empty_option' => '- Select -',
    '#default_value' => $bundle_default,
    '#ajax' => array(
      'callback' => 'personalize_view_modes_form_ajax_callback',
    ),
    '#states' => array(
      'disabled' => array(
        ':input[name="entity_type"]' => array('value' => ''),
      ),
    )
  );
  if(!isset($form['bundle']['#options'][$bundle_default])) {
    $form['bundle']['#default_value'] = '';
  }
  $form['entity_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Select the Entity to apply this to'),
    '#prefix' => '<div id="variant-set-entity-id-ajax-wrapper">',
    '#suffix' => '</div>',
    '#default_value' => $entity_id_default,
    '#states' => array(
      'disabled' => array(
        array(':input[name="entity_type"]' => array('value' => '')),
        array(':input[name="bundle"]' => array('value' => '')),
      ),
    ),
  );

  if($entity_type_default && $bundle_default) {
    $form['entity_id']['#autocomplete_path'] = 'personalize-view-modes/autocomplete/'.$entity_type_default . '/' . $bundle_default;
  }


  // Add a wrapper for the view modes and Add Another Tab button.
  $form['option_sets'] = array(
    '#tree' => FALSE,
    '#theme_wrappers' => array('fieldset'),
    '#title' => t('Variations'),
    '#attributes' => array('id' => 'option-view-mode-ajax-wrapper'),
//    '#weight' => -3,
  );

  $form['option_sets']['view_modes'] = array(
    '#tree' => TRUE,
    '#theme_wrappers' => array('container'),
    '#attributes' => array('id' => 'personalized-view-modes-variations'),
    '#theme' => 'personalize_view_modes_admin_form_draggable_view_modes',
  );

  $form['option_sets']['view_modes_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add another'),
    '#attributes' => array('class' => array('personalize-view-modes-add-view-mode'), 'title' => t('Click here to add more view modes.')),
    '#weight' => 1,
    '#submit' => array('personalize_view_modes_add_view_mode_submit'),
    '#ajax' => array(
      'callback' => 'personalize_view_modes_ajax_callback',
      'wrapper' => 'personalized-view-modes-variations',
      'effect' => 'fade',
    ),
    '#limit_validation_errors' => array(),
  );




  // If the "Add another" button was clicked, we need to increment the number of
  // view modes by one.
  $num_view_modes = count($variant_set->options);
  if (isset($form_state['num_view_modes']) && $form_state['num_view_modes'] > $num_view_modes) {
    $variant_set->options[] = array('option_label' => personalize_generate_option_label($num_view_modes));
  }
  $form_state['num_view_modes'] = count($variant_set->options);

  // Keep track of view mode options throughout Ajax submits.
  $form_state['variant_set'] =  $variant_set;

  // If the "Remove" button was clicked for a view mode, we need to remove that view mode
  // from the form.
  if (isset($form_state['to_remove'])) {
    unset($variant_set->options[$form_state['to_remove']]);
    unset($form_state['to_remove']);
    $form_state['num_view_modes']--;
  }

  // Add current tabs to the form.
  foreach ($variant_set->options as $delta => $block) {
    $block['delta'] = $delta;
    $form['option_sets']['view_modes'][$delta] = _personalize_view_modes_form($block, $variant_set);
  }
  // If there's only one block, it shouldn't be removeable.
  if (count($variant_set->options) == 1) {
    $form['option_sets']['view_modes'][$delta]['remove_' .$delta]['#access'] = FALSE;
  }

//  $form['original_block'] = array(
//    '#type' => 'value',
//    '#value' => isset($_GET['original_block']) ? $_GET['original_block'] : NULL,
//  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit_form'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/*
 * Build one row (one block) on the personalized_blocks admin form.
 *
 * @param array $block
 *  An array containing the details of this particular block.
 *
 * @param object $pblock
 *  An object representing the personalize_blocks instance that the blocks are
 *  being built for.
 */
function _personalize_view_modes_form(array $view_mode, $variant_set) {
  $form['#tree'] = TRUE;
  $delta = $view_mode['delta'];

  $form['option_label'] = array(
    '#type' => 'textfield',
    '#size' => '10',
    '#default_value' => isset($view_mode['option_label']) ? $view_mode['option_label'] : '',
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#default_value' => isset($view_mode['weight']) ? $view_mode['weight'] : $delta-100,
    '#delta' => 100,
  );
  $entity_type = isset($variant_set->data['entity_type']) ? $variant_set->data['entity_type'] : '';
  $bundle = isset($variant_set->data['bundle']) ? $variant_set->data['bundle'] : '';
  $view_mode_options = _personalize_view_modes_get_view_mode_options($entity_type, $bundle);
  $form['view_mode']  = array(
      '#title' => t('View Mode'),
      '#description' => t("Select the view mode to personalize. This view mode will serve as the control."),
      '#type' => 'select',
      '#default_value' => isset($view_mode['option_view_mode']) ? $view_mode['option_view_mode'] : '',
      '#options' => $view_mode_options,
      '#empty_value' => '',
      '#empty_option' => '- Select -',
      '#required' => TRUE,
      '#states' => array(
        'disabled' => array(
          array(':input[name="entity_type"]' => array('value' => '')),
          array(':input[name="bundle"]' => array('value' => '')),
        ),
      ),
    );
  $default = '';
//  if (isset($view_mode['bid'])) {
//    $default = $view_mode['bid'];
//  }
//  elseif (isset($_GET['personalize_blocks_new_block'])) {
//    $default = 'block_delta_' . $_GET['personalize_blocks_new_block'];
//    unset($_GET['personalize_blocks_new_block']);
//  }
//
//  $form['block'] = array(
//    '#type' => 'container',
//  );
//
//  $bid_id = "personalize_blocks_{$delta}_bid";
//  $form['block']['bid'] = array(
//    '#type' => 'select',
//    '#options' => array('' => t('Select a block...')) + personalize_blocks_get_blocks() + array('add' => t('Add a new block')),
//    '#default_value' => $default,
//    '#title' => t('Select a block'),
//    '#title_display' => 'invisible',
//    '#attributes' => array(
//      'id' => $bid_id,
//    ),
//  );
//
//  $form['block']['add'] = array(
//    '#type' => 'fieldset',
//    '#title' => t('Add a new block'),
//    '#states' => array(
//      'visible' => array(
//        '#' . $bid_id => array('value' => 'add'),
//      ),
//    ),
//  );
//  $form['block']['add'] += _personalize_blocks_add_block_form($form_state);
//
  $form['remove_' . $delta] = array(
    '#type' => 'submit',
    '#tag' => 'button',
    '#text' => t('Remove'),
    '#value' => 'Remove',
    '#name' => 'remove_' . $delta,
//    '#theme_wrappers' => array('personalize_html_tag'),
    '#attributes' => array(
      // The ID is necessary for the AJAX replace to function correctly. It
      // is fragile to declare it like this, essentially hard-coding the
      // #parents, but I know of no other way to do this programmatically.
      'id' => 'edit-blocks-' . $delta . '-remove',
      'class' => array('personalize-view-modes-delete-view-mode', 'form-submit',),
      'title' => t('Remove the block.'),
    ),

    '#submit' => array('personalize_view_modes_remove_view_mode_submit'),
    '#ajax' => array(
      'callback' => 'personalize_view_modes_ajax_callback',
      'wrapper' => 'personalized-view-modes-variations',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#limit_validation_errors' => array(),
  );
//  if (isset($view_mode['option_id'])) {
//    $form['option_id'] = array(
//      '#type' => 'value',
//      '#value' => $view_mode['option_id'],
//    );
//  }
//  else {
//    $form['option_label']['#attributes']['class'][] = 'personalize-blocks-add-option-label';
//    $form['block']['bid']['#attributes']['class'][] = 'personalize-blocks-add-block-select';
//  }
  return $form;
}


function personalize_view_modes_form_ajax_callback($form, &$form_state) {
  $commands = array();
  if($form_state['triggering_element']['#name'] == 'entity_type') {
//    if(!$form_state['values']['control_entity_type']) {
//      return;
//    }
    $bundle_options = array($form['bundle']['#empty_value'] => $form['bundle']['#empty_option']);
    $bundle_options +=  _personalize_view_modes_get_bundle_options($form_state['values']['entity_type']);
    if(!isset($bundle_options[$form_state['values']['bundle']])) {
      $form['bundle']['#default_value'] = '';
      $form_state['values']['bundle'] = '';
      $form['bundle']['#value'] = '';
      $commands[] = ajax_command_invoke('select[name="bundle"]', 'val', array(''));
    }
    $form['bundle']['#options'] = $bundle_options;
    $commands[] = ajax_command_invoke('select[name="bundle"]', 'trigger', array('change'));
    $commands[] = ajax_command_replace('#variant-set-bundle-ajax-wrapper', drupal_render($form['bundle']));
  }

  if($form_state['triggering_element']['#name'] == 'bundle' ) {
    if(!$form_state['values']['bundle']) {
      return;
    }

    $form['entity_id']['#autocomplete_path'] = 'personalize-view-modes/autocomplete/'.$form_state['values']['entity_type'] . '/' . $form_state['values']['bundle'];
    $commands[] = ajax_command_replace('#variant-set-entity-id-ajax-wrapper', drupal_render($form['entity_id']));

    $view_mode_options =  _personalize_view_modes_get_view_mode_options($form_state['values']['entity_type'], $form_state['values']['bundle']);

//    $form['control_view_mode']['#options'] = $view_mode_options;
//    $commands[] = ajax_command_replace('#control-view-mode-ajax-wrapper', drupal_render($form['control_view_mode']));
    foreach(element_children($form['option_sets']['view_modes']) as $index) {
//      dpm($elements);
      if(isset( $form['option_sets']['view_modes'][$index]['view_mode']['#options'])) {
       $form['option_sets']['view_modes'][$index]['view_mode']['#options'] = $view_mode_options;
      }
    }
    $commands[] = ajax_command_replace('#option-view-mode-ajax-wrapper', drupal_render($form['option_sets']));
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}
/**
 * Ajax callback for the add tab and remove tab buttons.
 */
function personalize_view_modes_ajax_callback($form, $form_state) {
  return $form['option_sets']['view_modes'];
}

/**
 * Submit handler for the "Add Tab" button.
 */
function personalize_view_modes_add_view_mode_submit($form, &$form_state) {
  // Increment the number of pages to be rendered.
  $form_state['num_view_modes']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Remove Tab" button.
 */
function personalize_view_modes_remove_view_mode_submit($form, &$form_state) {
  // Get the tab pid for the clicked button.
  $delta = $form_state['clicked_button']['#parents'][1];
  $form_state['to_remove'] = $delta;
  $form_state['rebuild'] = TRUE;
}

/**
 * Form callback for the Personalize View Modes delete form.
 */
function personalize_view_modes_view_mode_delete($form, &$form_state, $personalized_view_mode) {
  $form['osid'] = array('#type' => 'hidden', '#value' => $personalized_view_mode->osid);
  $form['title'] = array('#type' => 'hidden', '#value' => $personalized_view_mode->label);
  return confirm_form($form, t('Are you sure you want to delete the personalized_view_modes view mode %title?', array('%title' => $personalized_view_mode->label)), 'admin/structure/personalize/variations/personalize-view-modes', '', t('Delete'), t('Cancel'));
}


/**
 * Submit handler for Personalize View Modes delete form.
 */
function personalize_view_modes_view_mode_delete_submit($form, &$form_state) {
  personalize_option_set_delete($form_state['values']['osid']);
  drupal_set_message(t('The personalize_view_modes instance %name has been removed.', array('%name' => $form_state['values']['title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/personalize/variations/personalize-view-modes';
}


/**
 * Submit handler for personalize_view_mode admin page.
 */
function personalize_view_modes_form_validate($form, &$form_state) {
  //@todo: add validation to entity_id, entity_type, bundle and view modes.
}

/**
 * Theme function for personalize_blocks add page.
 * Theme the form elements for the blocks as draggable table rows.
 *
 * @ingroup themeable
 */
function theme_personalize_view_modes_admin_form_draggable_view_modes($variables) {
  $variables['view_modes']['#draggable'] = 1;
  return theme('personalize_view_modes_admin_form_view_modes', $variables);
}

/**
 * Theme function for personalize_blocks edit page.
 * Theme the form elements for the blocks as table rows.
 *
 * @ingroup themeable
 */
function theme_personalize_view_modes_admin_form_view_modes($variables) {
  $view_modes = $variables['view_modes'];
  $draggable = isset($view_modes['#draggable']) && $view_modes['#draggable'];
  $rows = array();
  $header = array(
    t('Variation label'),
  );
  if ($draggable) {
    drupal_add_tabledrag('personalize-view-modes-table', 'order', 'sibling', 'personalized-view-modes-weight');
    $header[] = t('Weight');
  }
  $header = array_merge($header, array(
    t('View Mode'),
    t('Operations'),
  ));
  foreach (element_children($view_modes) as $key) {
    $view_mode = &$view_modes[$key];
    if ($draggable) {
      $view_mode['weight']['#attributes']['class'] = array('personalized-view-modes-weight');
    }

    // tab settings fields
    $fields = array(
      array('data' => drupal_render($view_mode['option_label']), 'class' => array('personalized-view-mode-option-name')),
    );

    if ($draggable) {
      $fields[] = array('data' => drupal_render($view_mode['weight']), 'class' => array('personalized-view-modes-weight'));
    }
    $fields = array_merge($fields, array(
      array('data' => drupal_render($view_mode['view_mode']), 'class' => array('personalized-view-mode-view-mode')),
      array('data' => drupal_render($view_mode['remove_' . $key]), 'class' => array('personalize-view-mode-remove'))
    ));

    // Build the table row.
    $row = array(
      'data' => $fields,
      'class' => $draggable ? array('draggable') : array(),
    );

    // Add additional attributes to the row, such as a class for this row.
    if (isset($view_mode['#attributes'])) {
      $row = array_merge($row, $view_mode['#attributes']);
    }
    $rows[] = $row;
  }

  $build['personalized_view_mode'] = array(
    '#theme' => 'table',
    '#sticky' => FALSE,
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'personalize-view-modes-table'),
    '#weight' => -1,
  );

  $output = drupal_render($build);
  return $output;
}

/**
 * Submit handler for personalize_view_mode admin page.
 */
function personalize_view_modes_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op != t('Save')) {
    return;
  }
  $options = array();
  foreach ($form_state['values']['view_modes'] as $view_mode) {
    $options[] = array(
      'option_label' => $view_mode['option_label'],
      'option_view_mode' => $view_mode['view_mode'],
    );
  }

  // Save consistent settings to all options.
  $result = preg_match('/\[([0-9]+)\]$/', $form_state['values']['entity_id'], $matches);
  foreach($options as $i => $option) {
    $options[$i]['option_entity_type'] = $form_state['values']['entity_type'];
    $options[$i]['option_bundle'] = $form_state['values']['bundle'];
    $options[$i]['option_entity_id'] = end($matches);
  }

  $values = $form_state['values'];
  $option_set = $form_state['variant_set'];
  $option_set->agent = $values['agent_select'];
  $option_set->options = $options;
  $option_set->data['entity_id'] = end($matches);
  $option_set->executor = 'personalizeViewMode';

  if (isset($values['osid'])) {
    $option_set->osid = $values['osid'];
  }

  personalize_option_set_save($option_set);
  drupal_set_message(t('The option set was saved.'));
  $form_state['redirect'] = 'admin/structure/personalize/variations/personalize-view-modes';
}

/**
 * Return a list of qualified entity types.
 */
function _personalize_view_modes_get_entity_type_options() {
  $entity_types = entity_get_info();
  $options = array();
  foreach(_personalize_view_modes_get_all_map() as $entity_type_name => $bundles) {
    $options[$entity_type_name] = $entity_types[$entity_type_name]['label'];
  }
  return $options;
}

/**
 * Return a list of qualified bundles.
 */
function _personalize_view_modes_get_bundle_options($entity_type = '') {
  $entity_types = entity_get_info();
  $options = array();
  $map = _personalize_view_modes_get_all_map();
  if($entity_type) {
    if(isset($map[$entity_type])) {
      foreach($map[$entity_type] as $bundle_name => $view_modes) {
        $options[$bundle_name] = $entity_types[$entity_type]['bundles'][$bundle_name]['label'];
      }
    }
  } else {
    foreach($map as $entity_type => $bundles) {
      foreach($bundles as $bundle_name => $view_modes) {
        $options[$bundle_name] = $entity_types[$entity_type]['bundles'][$bundle_name]['label'];
      }
    }
  }
  return $options;
}

/**
 * Return a list of qualified view_modes.
 */
function _personalize_view_modes_get_view_mode_options($entity_type = '', $bundle = '') {
  $options = array();
  $map = _personalize_view_modes_get_all_map();
  if($entity_type && $bundle) {
    if(isset($map[$entity_type][$bundle])) {
      $options = $map[$entity_type][$bundle];
    }
  } elseif($entity_type) {
    if(isset($map[$entity_type])) {
      foreach($map[$entity_type] as $view_modes) {
        $options += $view_modes;
      }
    }
  } else {
    foreach($map as $entity_type => $bundles) {
      foreach($bundles as $bundle_name => $view_modes) {
        $options += $view_modes;
      }
    }
  }
  return $options;
}

/**
 * Iterate through all entity type bundles and return map with qualified view modes.
 */
function _personalize_view_modes_get_all_map() {
  $map = &drupal_static(__FUNCTION__, array());
  if(empty($map)) {
    foreach (entity_get_info() as $entity_type_name => $entity_type_info) {
      if (!$entity_type_info['fieldable']) {
        continue;
      }
      foreach ($entity_type_info['bundles'] as $bundle_name => $bundle_info) {
        $active_view_modes = array();
        foreach (field_view_mode_settings($entity_type_name, $bundle_name) as $view_modes_name => $settings) {
          if ($settings['custom_settings'] && isset($entity_type_info['view modes'][$view_modes_name])) {
            $active_view_modes[$view_modes_name] = $entity_type_info['view modes'][$view_modes_name]['label'];
          }
        }
        if (count($active_view_modes) > 1) {
          foreach ($active_view_modes as $view_modes_name => $view_modes_label) {
            $map[$entity_type_name][$bundle_name][$view_modes_name] = $view_modes_label;
          }
        }
      }
    }
  }
  return $map;
}

/**
 * Helper function to convert the data on admin form into personalized_view_mode presentation.
 *
 * @param $form_state
 *   The current form state to read data from.
 */
function _personalize_view_modes_form_to_personalized_view_mode($form_state) {
  $values = $form_state['values'];
  $option_set_values = array();
  if (!empty($form_state['values']['option_sets'])) {
    foreach ($form_state['values']['option_sets'] as $i => $view_mode) {
      $option_set_values[$i] = $view_mode;
      $weight[$i] = $view_mode['weight'];
    }
    array_multisort($weight, SORT_ASC, $option_set_values);
  }
  $options = array();
  foreach ($option_set_values as $view_mode) {
//    $options[] = array(
//      'option_label' => $view_mode,
//      'option_view_mode' => $form_state['values']['control_view_mode'],
//    );
//    $option = array(
//      'option_label' => $block['option_label'],
//      'bid' => $block['block']['bid'],
//    );
//    if (isset($block['option_id'])) {
//      $option['option_id'] = $block['option_id'];
//    }
//    $options[] = $option;
  }
  $variant_set = $form_state['variant_set'];
  $variant_set->label = $values['title'];
  $variant_set->plugin = 'view_modes';
  $variant_set->agent = $values['agent'];
  $variant_set->options = $options;
  $variant_set->executor = 'personalizeViewMode';
  $variant_set->data['entity_type'] = $form_state['values']['entity_type'];
  $variant_set->data['bundle'] = $form_state['values']['bundle'];
  $variant_set->data['entity_id'] = $form_state['values']['entity_id'];

  if (isset($values['osid'])) {
    $variant_set->osid = $values['osid'];
  }

  return $variant_set;
}